/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "ixion/model_context.hpp"
#include "ixion/formula_name_resolver.hpp"
#include "ixion/matrix.hpp"
#include "ixion/config.hpp"
#include "ixion/interface/session_handler.hpp"
#include "ixion/interface/table_handler.hpp"
#include "ixion/dirty_cell_tracker.hpp"
#include "ixion/formula_result.hpp"
#include "ixion/formula.hpp"
#include "ixion/model_iterator.hpp"

#include "workbook.hpp"
#include "model_types.hpp"
#include "calc_status.hpp"

#include <sstream>
#include <unordered_map>
#include <map>
#include <vector>
#include <deque>
#include <iostream>
#include <cstring>

#define DEBUG_MODEL_CONTEXT 0

using namespace std;

namespace ixion {

namespace {

model_context::session_handler_factory dummy_session_handler_factory;

} // anonymous namespace

class model_context_impl
{
    typedef std::vector<std::string> strings_type;
    typedef std::vector<std::unique_ptr<std::string>> string_pool_type;
    typedef std::unordered_map<mem_str_buf, string_id_t, mem_str_buf::hash> string_map_type;

public:
    model_context_impl() = delete;
    model_context_impl(const model_context_impl&) = delete;
    model_context_impl& operator= (model_context_impl) = delete;

    model_context_impl(model_context& parent) :
        m_parent(parent),
        m_tracker(parent),
        mp_table_handler(nullptr),
        mp_session_factory(&dummy_session_handler_factory)
    {
    }

    ~model_context_impl() {}

    const config& get_config() const
    {
        return m_config;
    }

    void set_config(const config& cfg)
    {
        m_config = cfg;
    }

    dirty_cell_tracker& get_cell_tracker()
    {
        return m_tracker;
    }

    const dirty_cell_tracker& get_cell_tracker() const
    {
        return m_tracker;
    }

    std::unique_ptr<iface::session_handler> create_session_handler()
    {
        return mp_session_factory->create();
    }

    void set_session_handler_factory(model_context::session_handler_factory* factory)
    {
        mp_session_factory = factory;
    }

    iface::table_handler* get_table_handler()
    {
        return mp_table_handler;
    }

    const iface::table_handler* get_table_handler() const
    {
        return mp_table_handler;
    }

    void set_table_handler(iface::table_handler* handler)
    {
        mp_table_handler = handler;
    }

    void erase_cell(const abs_address_t& addr);
    void set_numeric_cell(const abs_address_t& addr, double val);
    void set_boolean_cell(const abs_address_t& addr, bool val);
    void set_string_cell(const abs_address_t& addr, const char* p, size_t n);
    void set_string_cell(const abs_address_t& addr, string_id_t identifier);
    void fill_down_cells(const abs_address_t& src, size_t n_dst);
    void set_formula_cell(const abs_address_t& addr, formula_tokens_t tokens);
    void set_formula_cell(const abs_address_t& addr, const formula_tokens_store_ptr_t& tokens);
    void set_grouped_formula_cells(const abs_range_t& group_range, formula_tokens_t tokens);

    abs_range_t get_data_range(sheet_t sheet) const;

    bool is_empty(const abs_address_t& addr) const;
    celltype_t get_celltype(const abs_address_t& addr) const;
    double get_numeric_value(const abs_address_t& addr) const;
    double get_numeric_value_nowait(const abs_address_t& addr) const;
    bool get_boolean_value(const abs_address_t& addr) const;
    string_id_t get_string_identifier(const abs_address_t& addr) const;
    string_id_t get_string_identifier_nowait(const abs_address_t& addr) const;
    string_id_t get_string_identifier(const char* p, size_t n) const;
    const formula_cell* get_formula_cell(const abs_address_t& addr) const;
    formula_cell* get_formula_cell(const abs_address_t& addr);

    void set_named_expression(const char* p, size_t n, std::unique_ptr<formula_tokens_t>&& expr);
    void set_named_expression(sheet_t sheet, const char* p, size_t n, std::unique_ptr<formula_tokens_t>&& expr);

    const formula_tokens_t* get_named_expression(const string& name) const;
    const formula_tokens_t* get_named_expression(sheet_t sheet, const string& name) const;

    sheet_t get_sheet_index(const char* p, size_t n) const;
    std::string get_sheet_name(sheet_t sheet) const;
    rc_size_t get_sheet_size(sheet_t sheet) const;
    size_t get_sheet_count() const;
    sheet_t append_sheet(const char* p, size_t n, row_t row_size, col_t col_size);
    sheet_t append_sheet(std::string&& name, row_t row_size, col_t col_size);

    void set_cell_values(sheet_t sheet, std::initializer_list<model_context::input_row>&& rows);

    string_id_t append_string(const char* p, size_t n);
    string_id_t add_string(const char* p, size_t n);
    const std::string* get_string(string_id_t identifier) const;
    size_t get_string_count() const;
    void dump_strings() const;

    const column_store_t* get_column(sheet_t sheet, col_t col) const;
    const column_stores_t* get_columns(sheet_t sheet) const;

    double count_range(const abs_range_t& range, const values_t& values_type) const;

    abs_address_set_t get_all_formula_cells() const;

    bool empty() const;

    const worksheet* fetch_sheet(sheet_t sheet_index) const;

private:
    model_context& m_parent;

    workbook m_sheets;

    config m_config;
    dirty_cell_tracker m_tracker;
    iface::table_handler* mp_table_handler;
    detail::named_expressions_t m_named_expressions;

    model_context::session_handler_factory* mp_session_factory;

    strings_type m_sheet_names; ///< index to sheet name map.
    string_pool_type m_strings;
    string_map_type m_string_map;
    string m_empty_string;
};

void model_context_impl::set_named_expression(const char* p, size_t n, std::unique_ptr<formula_tokens_t>&& expr)
{
    string name(p, n);
    m_named_expressions.insert(
        detail::named_expressions_t::value_type(std::move(name), std::move(expr)));
}

void model_context_impl::set_named_expression(
    sheet_t sheet, const char* p, size_t n, std::unique_ptr<formula_tokens_t>&& expr)
{
    detail::named_expressions_t& ns = m_sheets.at(sheet).get_named_expressions();
    string name(p, n);
    ns.insert(
        detail::named_expressions_t::value_type(std::move(name), std::move(expr)));
}

const formula_tokens_t* model_context_impl::get_named_expression(const string& name) const
{
    detail::named_expressions_t::const_iterator itr = m_named_expressions.find(name);
    return itr == m_named_expressions.end() ? nullptr : itr->second.get();
}

const formula_tokens_t* model_context_impl::get_named_expression(sheet_t sheet, const string& name) const
{
    const worksheet* ws = fetch_sheet(sheet);

    if (ws)
    {
        const detail::named_expressions_t& ns = ws->get_named_expressions();
        auto it = ns.find(name);
        if (it != ns.end())
            return it->second.get();
    }

    // Search the global scope if not found in the sheet local scope.
    return get_named_expression(name);
}

sheet_t model_context_impl::get_sheet_index(const char* p, size_t n) const
{
    strings_type::const_iterator itr_beg = m_sheet_names.begin(), itr_end = m_sheet_names.end();
    for (strings_type::const_iterator itr = itr_beg; itr != itr_end; ++itr)
    {
        const std::string& s = *itr;
        if (s.empty())
            continue;

        mem_str_buf s1(&s[0], s.size()), s2(p, n);
        if (s1 == s2)
            return static_cast<sheet_t>(std::distance(itr_beg, itr));
    }
    return invalid_sheet;
}

std::string model_context_impl::get_sheet_name(sheet_t sheet) const
{
    if (m_sheet_names.size() <= static_cast<size_t>(sheet))
        return std::string();

    return m_sheet_names[sheet];
}

rc_size_t model_context_impl::get_sheet_size(sheet_t sheet) const
{
    return m_sheets.at(sheet).get_sheet_size();
}

size_t model_context_impl::get_sheet_count() const
{
    return m_sheets.size();
}

sheet_t model_context_impl::append_sheet(
    const char* p, size_t n, row_t row_size, col_t col_size)
{
    // Check if the new sheet name already exists.
    string new_name(p, n);
    strings_type::const_iterator it =
        std::find(m_sheet_names.begin(), m_sheet_names.end(), new_name);
    if (it != m_sheet_names.end())
    {
        // This sheet name is already taken.
        ostringstream os;
        os << "Sheet name '" << new_name << "' already exists.";
        throw model_context_error(os.str(), model_context_error::sheet_name_conflict);
    }

    // index of the new sheet.
    sheet_t sheet_index = m_sheets.size();

    m_sheet_names.emplace_back(p, n);
    m_sheets.push_back(row_size, col_size);
    return sheet_index;
}

sheet_t model_context_impl::append_sheet(
    std::string&& name, row_t row_size, col_t col_size)
{
    // Check if the new sheet name already exists.
    strings_type::const_iterator it =
        std::find(m_sheet_names.begin(), m_sheet_names.end(), name);
    if (it != m_sheet_names.end())
    {
        // This sheet name is already taken.
        ostringstream os;
        os << "Sheet name '" << name << "' already exists.";
        throw model_context_error(os.str(), model_context_error::sheet_name_conflict);
    }

    // index of the new sheet.
    sheet_t sheet_index = m_sheets.size();

    m_sheet_names.push_back(std::move(name));
    m_sheets.push_back(row_size, col_size);
    return sheet_index;
}

void model_context_impl::set_cell_values(sheet_t sheet, std::initializer_list<model_context::input_row>&& rows)
{
    abs_address_t pos;
    pos.sheet = sheet;
    pos.row = 0;
    pos.column = 0;

    // TODO : This function is not optimized for speed as it is mainly for
    // convenience.  Decide if we need to optimize this later.

    for (const model_context::input_row& row : rows)
    {
        pos.column = 0;

        for (const model_context::input_cell& c : row.cells())
        {
            switch (c.type)
            {
                case celltype_t::numeric:
                    set_numeric_cell(pos, c.value.numeric);
                    break;
                case celltype_t::string:
                    set_string_cell(pos, c.value.string, std::strlen(c.value.string));
                    break;
                case celltype_t::boolean:
                    set_boolean_cell(pos, c.value.boolean);
                    break;
                default:
                    ;
            }

            ++pos.column;
        }

        ++pos.row;
    }
}

string_id_t model_context_impl::append_string(const char* p, size_t n)
{
    if (!p || !n)
        // Never add an empty or invalid string.
        return empty_string_id;

    string_id_t str_id = m_strings.size();
    m_strings.push_back(make_unique<std::string>(p, n));
    p = m_strings.back()->data();
    mem_str_buf key(p, n);
    m_string_map.insert(string_map_type::value_type(key, str_id));
    return str_id;
}

string_id_t model_context_impl::add_string(const char* p, size_t n)
{
    string_map_type::iterator itr = m_string_map.find(mem_str_buf(p, n));
    if (itr != m_string_map.end())
        return itr->second;

    return append_string(p, n);
}

const std::string* model_context_impl::get_string(string_id_t identifier) const
{
    if (identifier == empty_string_id)
        return &m_empty_string;

    if (identifier >= m_strings.size())
        return nullptr;

    return m_strings[identifier].get();
}

size_t model_context_impl::get_string_count() const
{
    return m_strings.size();
}

void model_context_impl::dump_strings() const
{
    {
        cout << "string count: " << m_strings.size() << endl;
        auto it = m_strings.begin(), ite = m_strings.end();
        for (string_id_t sid = 0; it != ite; ++it, ++sid)
        {
            const std::string& s = **it;
            cout << "* " << sid << ": '" << s << "' (" << (void*)s.data() << ")" << endl;
        }
    }

    {
        cout << "string map count: " << m_string_map.size() << endl;
        auto it = m_string_map.begin(), ite = m_string_map.end();
        for (; it != ite; ++it)
        {
            mem_str_buf key = it->first;
            cout << "* key: '" << key << "' (" << (void*)key.get() << "; " << key.size() << "), value: " << it->second << endl;
        }
    }
}

const column_store_t* model_context_impl::get_column(sheet_t sheet, col_t col) const
{
    if (static_cast<size_t>(sheet) >= m_sheets.size())
        return nullptr;

    const worksheet& sh = m_sheets[sheet];

    if (static_cast<size_t>(col) >= sh.size())
        return nullptr;

    return &sh[col];
}

const column_stores_t* model_context_impl::get_columns(sheet_t sheet) const
{
    if (static_cast<size_t>(sheet) >= m_sheets.size())
        return nullptr;

    const worksheet& sh = m_sheets[sheet];
    return &sh.get_columns();
}

namespace {

double count_formula_block(
    const column_store_t::const_iterator& itb, size_t offset, size_t len, const values_t& vt)
{
    double ret = 0.0;

    // Inspect each formula cell individually.
    formula_cell** pp = &formula_element_block::at(*itb->data, offset);
    formula_cell** pp_end = pp + len;
    for (; pp != pp_end; ++pp)
    {
        const formula_cell& fc = **pp;
        formula_result res = fc.get_result_cache();

        switch (res.get_type())
        {
            case formula_result::result_type::value:
                if (vt.is_numeric())
                    ++ret;
                break;
            case formula_result::result_type::string:
                if (vt.is_string())
                    ++ret;
                break;
            case formula_result::result_type::error:
                // TODO : how do we handle error formula cells?
                break;
            case formula_result::result_type::matrix:
                // TODO : ditto
                break;
        }
    }

    return ret;
}

}

double model_context_impl::count_range(const abs_range_t& range, const values_t& values_type) const
{
    if (m_sheets.empty())
        return 0.0;

    double ret = 0.0;
    sheet_t last_sheet = range.last.sheet;
    if (static_cast<size_t>(last_sheet) >= m_sheets.size())
        last_sheet = m_sheets.size() - 1;

    for (sheet_t sheet = range.first.sheet; sheet <= last_sheet; ++sheet)
    {
        const worksheet& ws = m_sheets.at(sheet);
        for (col_t col = range.first.column; col <= range.last.column; ++col)
        {
            const column_store_t& cs = ws.at(col);
            row_t cur_row = range.first.row;
            column_store_t::const_position_type pos = cs.position(cur_row);
            column_store_t::const_iterator itb = pos.first; // block iterator
            column_store_t::const_iterator itb_end = cs.end();
            size_t offset = pos.second;
            if (itb == itb_end)
                continue;

            bool cont = true;
            while (cont)
            {
                // remaining length of current block.
                size_t len = itb->size - offset;
                row_t last_row = cur_row + len - 1;

                if (last_row >= range.last.row)
                {
                    last_row = range.last.row;
                    len = last_row - cur_row + 1;
                    cont = false;
                }

                bool match = false;

                switch (itb->type)
                {
                    case element_type_numeric:
                        match = values_type.is_numeric();
                        break;
                    case element_type_boolean:
                        match = values_type.is_boolean();
                        break;
                    case element_type_string:
                        match = values_type.is_string();
                        break;
                    case element_type_empty:
                        match = values_type.is_empty();
                        break;
                    case element_type_formula:
                        ret += count_formula_block(itb, offset, len, values_type);
                        break;
                    default:
                    {
                        std::ostringstream os;
                        os << __FUNCTION__ << ": unhandled block type (" << itb->type << ")";
                        throw general_error(os.str());
                    }
                }

                if (match)
                    ret += len;

                // Move to the next block.
                cur_row = last_row + 1;
                ++itb;
                offset = 0;
                if (itb == itb_end)
                    cont = false;
            }
        }
    }

    return ret;
}

abs_address_set_t model_context_impl::get_all_formula_cells() const
{
    abs_address_set_t cells;

    for (size_t sid = 0; sid < m_sheets.size(); ++sid)
    {
        const worksheet& sh = m_sheets[sid];
        for (size_t cid = 0; cid < sh.size(); ++cid)
        {
            const column_store_t& col = sh[cid];
            column_store_t::const_iterator it = col.begin();
            column_store_t::const_iterator ite = col.end();
            for (; it != ite; ++it)
            {
                if (it->type != element_type_formula)
                    continue;

                row_t row_id = it->position;
                abs_address_t pos(sid, row_id, cid);
                for (size_t i = 0; i < it->size; ++i, ++pos.row)
                    cells.insert(pos);
            }
        }
    }

    return cells;
}

bool model_context_impl::empty() const
{
    return m_sheets.empty();
}

const worksheet* model_context_impl::fetch_sheet(sheet_t sheet_index) const
{
    if (sheet_index < 0 || m_sheets.size() <= size_t(sheet_index))
        return nullptr;

    return &m_sheets[sheet_index];
}

void model_context_impl::erase_cell(const abs_address_t& addr)
{
    worksheet& sheet = m_sheets.at(addr.sheet);
    column_store_t& col_store = sheet.at(addr.column);
    column_store_t::iterator& pos_hint = sheet.get_pos_hint(addr.column);
    pos_hint = col_store.set_empty(addr.row, addr.row);
}

void model_context_impl::set_numeric_cell(const abs_address_t& addr, double val)
{
    worksheet& sheet = m_sheets.at(addr.sheet);
    column_store_t& col_store = sheet.at(addr.column);
    column_store_t::iterator& pos_hint = sheet.get_pos_hint(addr.column);
    pos_hint = col_store.set(pos_hint, addr.row, val);
}

void model_context_impl::set_boolean_cell(const abs_address_t& addr, bool val)
{
    worksheet& sheet = m_sheets.at(addr.sheet);
    column_store_t& col_store = sheet.at(addr.column);
    column_store_t::iterator& pos_hint = sheet.get_pos_hint(addr.column);
    pos_hint = col_store.set(pos_hint, addr.row, val);
}

void model_context_impl::set_string_cell(const abs_address_t& addr, const char* p, size_t n)
{
    worksheet& sheet = m_sheets.at(addr.sheet);
    string_id_t str_id = add_string(p, n);
    column_store_t& col_store = sheet.at(addr.column);
    column_store_t::iterator& pos_hint = sheet.get_pos_hint(addr.column);
    pos_hint = col_store.set(pos_hint, addr.row, str_id);
}

void model_context_impl::fill_down_cells(const abs_address_t& src, size_t n_dst)
{
    if (!n_dst)
        // Destination cell length is 0.  Nothing to copy to.
        return;

    worksheet& sheet = m_sheets.at(src.sheet);
    column_store_t& col_store = sheet.at(src.column);
    column_store_t::iterator& pos_hint = sheet.get_pos_hint(src.column);

    column_store_t::const_position_type pos = col_store.position(pos_hint, src.row);
    auto it = pos.first; // block iterator

    switch (it->type)
    {
        case element_type_numeric:
        {
            double v = col_store.get<numeric_element_block>(pos);
            std::vector<double> vs(n_dst, v);
            pos_hint = col_store.set(pos_hint, src.row+1, vs.begin(), vs.end());
            break;
        }
        case element_type_boolean:
        {
            bool b = col_store.get<boolean_element_block>(pos);
            std::deque<bool> vs(n_dst, b);
            pos_hint = col_store.set(pos_hint, src.row+1, vs.begin(), vs.end());
            break;
        }
        case element_type_string:
        {
            string_id_t sid = col_store.get<string_element_block>(pos);
            std::vector<string_id_t> vs(n_dst, sid);
            pos_hint = col_store.set(pos_hint, src.row+1, vs.begin(), vs.end());
            break;
        }
        case element_type_empty:
        {
            size_t start_pos = src.row + 1;
            size_t end_pos = start_pos + n_dst - 1;
            pos_hint = col_store.set_empty(pos_hint, start_pos, end_pos);
            break;
        }
        case element_type_formula:
            // TODO : support this.
            throw not_implemented_error("filling down of a formula cell is not yet supported.");
        default:
        {
            std::ostringstream os;
            os << __FUNCTION__ << ": unhandled block type (" << it->type << ")";
            throw general_error(os.str());
        }
    }
}

void model_context_impl::set_string_cell(const abs_address_t& addr, string_id_t identifier)
{
    worksheet& sheet = m_sheets.at(addr.sheet);
    column_store_t& col_store = sheet.at(addr.column);
    column_store_t::iterator& pos_hint = sheet.get_pos_hint(addr.column);
    pos_hint = col_store.set(pos_hint, addr.row, identifier);
}

void model_context_impl::set_formula_cell(const abs_address_t& addr, formula_tokens_t tokens)
{
    formula_tokens_store_ptr_t ts = formula_tokens_store::create();
    ts->get() = std::move(tokens);

    set_formula_cell(addr, ts);
}

void model_context_impl::set_formula_cell(
    const abs_address_t& addr, const formula_tokens_store_ptr_t& tokens)
{
    std::unique_ptr<formula_cell> fcell = ixion::make_unique<formula_cell>(tokens);

    worksheet& sheet = m_sheets.at(addr.sheet);
    column_store_t& col_store = sheet.at(addr.column);
    column_store_t::iterator& pos_hint = sheet.get_pos_hint(addr.column);
    pos_hint = col_store.set(pos_hint, addr.row, fcell.release());
}

void model_context_impl::set_grouped_formula_cells(
    const abs_range_t& group_range, formula_tokens_t tokens)
{
    const abs_address_t& top_left = group_range.first;

    rc_size_t group_size;
    group_size.row    = group_range.last.row - group_range.first.row + 1;
    group_size.column = group_range.last.column - group_range.first.column + 1;

    formula_tokens_store_ptr_t ts = formula_tokens_store::create();
    ts->get() = std::move(tokens);

    calc_status_ptr_t cs(new calc_status(group_size));

    worksheet& sheet = m_sheets.at(top_left.sheet);

    for (col_t col_offset = 0; col_offset < group_size.column; ++col_offset)
    {
        col_t col = top_left.column + col_offset;
        column_store_t& col_store = sheet.at(col);
        column_store_t::iterator& pos_hint = sheet.get_pos_hint(col);

        for (row_t row_offset = 0; row_offset < group_size.row; ++row_offset)
        {
            row_t row = top_left.row + row_offset;
            pos_hint = col_store.set(pos_hint, row, new formula_cell(row_offset, col_offset, cs, ts));
        }
    }
}

abs_range_t model_context_impl::get_data_range(sheet_t sheet) const
{
    const worksheet& cols = m_sheets.at(sheet);
    size_t col_size = cols.size();
    if (!col_size)
        return abs_range_t(abs_range_t::invalid);

    row_t row_size = cols[0].size();
    if (!row_size)
        return abs_range_t(abs_range_t::invalid);

    abs_range_t range;
    range.first.column = 0;
    range.first.row = row_size-1;
    range.first.sheet = sheet;
    range.last.column = -1; // if this stays -1 all columns are empty.
    range.last.row = 0;
    range.last.sheet = sheet;

    for (size_t i = 0; i < col_size; ++i)
    {
        const column_store_t& col = cols[i];
        if (col.empty())
        {
            if (range.last.column < 0)
                ++range.first.column;
            continue;
        }

        if (range.first.row > 0)
        {
            // First non-empty row.

            column_store_t::const_iterator it = col.begin(), it_end = col.end();
            assert(it != it_end);
            if (it->type == element_type_empty)
            {
                // First block is empty.
                row_t offset = it->size;
                ++it;
                if (it == it_end)
                {
                    // The whole column is empty.
                    if (range.last.column < 0)
                        ++range.first.column;
                    continue;
                }

                assert(it->type != element_type_empty);
                if (range.first.row > offset)
                    range.first.row = offset;
            }
            else
                // Set the first row to 0, and lock it.
                range.first.row = 0;
        }

        if (range.last.row < (row_size-1))
        {
            // Last non-empty row.

            column_store_t::const_reverse_iterator it = col.rbegin(), it_end = col.rend();
            assert(it != it_end);
            if (it->type == element_type_empty)
            {
                // Last block is empty.
                size_t size_last_block = it->size;
                ++it;
                if (it == it_end)
                {
                    // The whole column is empty.
                    if (range.last.column < 0)
                        ++range.first.column;
                    continue;
                }

                assert(it->type != element_type_empty);
                row_t last_data_row = static_cast<row_t>(col.size() - size_last_block - 1);
                if (range.last.row < last_data_row)
                    range.last.row = last_data_row;
            }
            else
                // Last block is not empty.
                range.last.row = row_size - 1;
        }

        // Check if the column contains at least one non-empty cell.
        if (col.block_size() > 1 || !col.is_empty(0))
            range.last.column = i;
    }

    if (range.last.column < 0)
        // No data column found.  The whole sheet is empty.
        return abs_range_t(abs_range_t::invalid);

    return range;
}

bool model_context_impl::is_empty(const abs_address_t& addr) const
{
    return m_sheets.at(addr.sheet).at(addr.column).is_empty(addr.row);
}

celltype_t model_context_impl::get_celltype(const abs_address_t& addr) const
{
    mdds::mtv::element_t gmcell_type =
        m_sheets.at(addr.sheet).at(addr.column).get_type(addr.row);
    switch (gmcell_type)
    {
        case element_type_empty:
            return celltype_t::empty;
        case element_type_numeric:
            return celltype_t::numeric;
        case element_type_boolean:
            return celltype_t::boolean;
        case element_type_string:
            return celltype_t::string;
        case element_type_formula:
            return celltype_t::formula;
        default:
        {
            std::ostringstream os;
            os << "ixion::model_context_impl::get_celltype: unknown cell type (" << gmcell_type << ")";
            throw general_error(os.str());
        }
    }

    return celltype_t::unknown;
}

double model_context_impl::get_numeric_value(const abs_address_t& addr) const
{
    const column_store_t& col_store = m_sheets.at(addr.sheet).at(addr.column);
    switch (col_store.get_type(addr.row))
    {
        case element_type_numeric:
            return col_store.get<double>(addr.row);
        case element_type_boolean:
            return col_store.get<bool>(addr.row);
        case element_type_formula:
        {
            const formula_cell* p = col_store.get<formula_cell*>(addr.row);
            return p->get_value();
        }
        break;
        default:
            ;
    }
    return 0.0;
}

double model_context_impl::get_numeric_value_nowait(const abs_address_t& addr) const
{
    const column_store_t& col_store = m_sheets.at(addr.sheet).at(addr.column);
    switch (col_store.get_type(addr.row))
    {
        case element_type_numeric:
            return col_store.get<double>(addr.row);
        case element_type_boolean:
            return col_store.get<bool>(addr.row);
        case element_type_formula:
        {
            const formula_cell* p = col_store.get<formula_cell*>(addr.row);
            return p->get_value_nowait();
        }
        break;
        default:
            ;
    }
    return 0.0;
}

bool model_context_impl::get_boolean_value(const abs_address_t& addr) const
{
    const column_store_t& col_store = m_sheets.at(addr.sheet).at(addr.column);
    switch (col_store.get_type(addr.row))
    {
        case element_type_numeric:
            return col_store.get<double>(addr.row) != 0.0 ? true : false;
        case element_type_boolean:
            return col_store.get<bool>(addr.row);
        case element_type_formula:
        {
            const formula_cell* p = col_store.get<formula_cell*>(addr.row);
            return p->get_value() != 0.0 ? true : false;
        }
        break;
        default:
            ;
    }
    return false;
}

string_id_t model_context_impl::get_string_identifier(const abs_address_t& addr) const
{
    const column_store_t& col_store = m_sheets.at(addr.sheet).at(addr.column);
    switch (col_store.get_type(addr.row))
    {
        case element_type_string:
            return col_store.get<string_id_t>(addr.row);
        default:
            ;
    }
    return empty_string_id;
}

string_id_t model_context_impl::get_string_identifier_nowait(const abs_address_t& addr) const
{
    const column_store_t& col_store = m_sheets.at(addr.sheet).at(addr.column);
    switch (col_store.get_type(addr.row))
    {
        case element_type_string:
            return col_store.get<string_id_t>(addr.row);
        case element_type_formula:
        {
            const formula_cell* p = col_store.get<formula_cell*>(addr.row);
            formula_result res_cache = p->get_result_cache_nowait();
            formula_result::result_type rt = res_cache.get_type();
            if (rt == formula_result::result_type::error &&
                res_cache.get_error() == formula_error_t::no_result_error)
                break;

            switch (rt)
            {
                case formula_result::result_type::string:
                    return res_cache.get_string();
                case formula_result::result_type::error:
                    // TODO : perhaps we should return the error string here.
                default:
                    ;
            }
            break;
        }
        default:
            ;
    }
    return empty_string_id;
}

string_id_t model_context_impl::get_string_identifier(const char* p, size_t n) const
{
    string_map_type::const_iterator it = m_string_map.find(mem_str_buf(p, n));
    return it == m_string_map.end() ? empty_string_id : it->second;
}

const formula_cell* model_context_impl::get_formula_cell(const abs_address_t& addr) const
{
    const column_store_t& col_store = m_sheets.at(addr.sheet).at(addr.column);
    if (col_store.get_type(addr.row) != element_type_formula)
        return NULL;

    return col_store.get<formula_cell*>(addr.row);
}

formula_cell* model_context_impl::get_formula_cell(const abs_address_t& addr)
{
    column_store_t& col_store = m_sheets.at(addr.sheet).at(addr.column);
    if (col_store.get_type(addr.row) != element_type_formula)
        return NULL;

    return col_store.get<formula_cell*>(addr.row);
}

model_context::input_cell::input_cell(nullptr_t) : type(celltype_t::empty) {}
model_context::input_cell::input_cell(bool b) : type(celltype_t::boolean)
{
    value.boolean = b;
}

model_context::input_cell::input_cell(const char* s) : type(celltype_t::string)
{
    value.string = s;
}

model_context::input_cell::input_cell(double v) : type(celltype_t::numeric)
{
    value.numeric = v;
}

model_context::input_cell::input_cell(const input_cell& other) :
    type(other.type)
{
    switch (type)
    {
        case celltype_t::numeric:
            value.numeric = other.value.numeric;
            break;
        case celltype_t::string:
            value.string = other.value.string;
            break;
        case celltype_t::boolean:
            value.boolean = other.value.boolean;
            break;
        default:
            ;
    }
}

model_context::input_row::input_row(std::initializer_list<input_cell> cells) :
    m_cells(std::move(cells)) { }

const std::initializer_list<model_context::input_cell>& model_context::input_row::cells() const
{
    return m_cells;
}

std::unique_ptr<iface::session_handler> model_context::session_handler_factory::create()
{
    return std::unique_ptr<iface::session_handler>();
}

model_context::session_handler_factory::~session_handler_factory() {}

model_context::model_context() :
    mp_impl(new model_context_impl(*this)) {}

model_context::~model_context()
{
}

const config& model_context::get_config() const
{
    return mp_impl->get_config();
}

dirty_cell_tracker& model_context::get_cell_tracker()
{
    return mp_impl->get_cell_tracker();
}

const dirty_cell_tracker& model_context::get_cell_tracker() const
{
    return mp_impl->get_cell_tracker();
}

void model_context::erase_cell(const abs_address_t& addr)
{
    mp_impl->erase_cell(addr);
}

void model_context::set_numeric_cell(const abs_address_t& addr, double val)
{
    mp_impl->set_numeric_cell(addr, val);
}

void model_context::set_boolean_cell(const abs_address_t& addr, bool val)
{
    mp_impl->set_boolean_cell(addr, val);
}

void model_context::set_string_cell(const abs_address_t& addr, const char* p, size_t n)
{
    mp_impl->set_string_cell(addr, p, n);
}

void model_context::fill_down_cells(const abs_address_t& src, size_t n_dst)
{
    mp_impl->fill_down_cells(src, n_dst);
}

void model_context::set_string_cell(const abs_address_t& addr, string_id_t identifier)
{
    mp_impl->set_string_cell(addr, identifier);
}

void model_context::set_formula_cell(const abs_address_t& addr, formula_tokens_t tokens)
{
    mp_impl->set_formula_cell(addr, std::move(tokens));
}

void model_context::set_formula_cell(
    const abs_address_t& addr, const formula_tokens_store_ptr_t& tokens)
{
    mp_impl->set_formula_cell(addr, tokens);
}

void model_context::set_grouped_formula_cells(
    const abs_range_t& group_range, formula_tokens_t tokens)
{
    mp_impl->set_grouped_formula_cells(group_range, std::move(tokens));
}

abs_range_t model_context::get_data_range(sheet_t sheet) const
{
    return mp_impl->get_data_range(sheet);
}

bool model_context::is_empty(const abs_address_t& addr) const
{
    return mp_impl->is_empty(addr);
}

celltype_t model_context::get_celltype(const abs_address_t& addr) const
{
    return mp_impl->get_celltype(addr);
}

double model_context::get_numeric_value(const abs_address_t& addr) const
{
    return mp_impl->get_numeric_value(addr);
}

bool model_context::get_boolean_value(const abs_address_t& addr) const
{
    return mp_impl->get_boolean_value(addr);
}

void model_context::set_config(const config& cfg)
{
    mp_impl->set_config(cfg);
}

double model_context::get_numeric_value_nowait(const abs_address_t& addr) const
{
    return mp_impl->get_numeric_value_nowait(addr);
}

string_id_t model_context::get_string_identifier(const abs_address_t& addr) const
{
    return mp_impl->get_string_identifier(addr);
}

string_id_t model_context::get_string_identifier_nowait(const abs_address_t& addr) const
{
    return mp_impl->get_string_identifier_nowait(addr);
}

string_id_t model_context::get_string_identifier(const char* p, size_t n) const
{
    return mp_impl->get_string_identifier(p, n);
}

const formula_cell* model_context::get_formula_cell(const abs_address_t& addr) const
{
    return mp_impl->get_formula_cell(addr);
}

formula_cell* model_context::get_formula_cell(const abs_address_t& addr)
{
    return mp_impl->get_formula_cell(addr);
}

double model_context::count_range(const abs_range_t& range, const values_t& values_type) const
{
    return mp_impl->count_range(range, values_type);
}

matrix model_context::get_range_value(const abs_range_t& range) const
{
    if (range.first.sheet != range.last.sheet)
        throw general_error("multi-sheet range is not allowed.");

    row_t rows = range.last.row - range.first.row + 1;
    col_t cols = range.last.column - range.first.column + 1;

    matrix ret(rows, cols);
    for (row_t i = 0; i < rows; ++i)
    {
        for (col_t j = 0; j < cols; ++j)
        {
            row_t row = i + range.first.row;
            col_t col = j + range.first.column;
            double val = get_numeric_value(abs_address_t(range.first.sheet, row, col));

            // TODO: we need to handle string types when that becomes available.
            ret.set(i, j, val);
        }
    }
    return ret;
}

std::unique_ptr<iface::session_handler> model_context::create_session_handler()
{
    return mp_impl->create_session_handler();
}

iface::table_handler* model_context::get_table_handler()
{
    return mp_impl->get_table_handler();
}

const iface::table_handler* model_context::get_table_handler() const
{
    return mp_impl->get_table_handler();
}

string_id_t model_context::append_string(const char* p, size_t n)
{
    return mp_impl->append_string(p, n);
}

string_id_t model_context::add_string(const char* p, size_t n)
{
    return mp_impl->add_string(p, n);
}

const std::string* model_context::get_string(string_id_t identifier) const
{
    return mp_impl->get_string(identifier);
}

sheet_t model_context::get_sheet_index(const char* p, size_t n) const
{
    return mp_impl->get_sheet_index(p, n);
}

std::string model_context::get_sheet_name(sheet_t sheet) const
{
    return mp_impl->get_sheet_name(sheet);
}

rc_size_t model_context::get_sheet_size(sheet_t sheet) const
{
    return mp_impl->get_sheet_size(sheet);
}

size_t model_context::get_sheet_count() const
{
    return mp_impl->get_sheet_count();
}

void model_context::set_named_expression(const char* p, size_t n, std::unique_ptr<formula_tokens_t>&& expr)
{
    mp_impl->set_named_expression(p, n, std::move(expr));
}

void model_context::set_named_expression(
    sheet_t sheet, const char* p, size_t n, std::unique_ptr<formula_tokens_t>&& expr)
{
    mp_impl->set_named_expression(sheet, p, n, std::move(expr));
}

const formula_tokens_t* model_context::get_named_expression(sheet_t sheet, const string& name) const
{
    return mp_impl->get_named_expression(sheet, name);
}

sheet_t model_context::append_sheet(const char* p, size_t n, row_t row_size, col_t col_size)
{
    return mp_impl->append_sheet(p, n, row_size, col_size);
}

sheet_t model_context::append_sheet(std::string name, row_t row_size, col_t col_size)
{
    return mp_impl->append_sheet(std::move(name), row_size, col_size);
}

void model_context::set_cell_values(sheet_t sheet, std::initializer_list<input_row> rows)
{
    mp_impl->set_cell_values(sheet, std::move(rows));
}

void model_context::set_session_handler_factory(session_handler_factory* factory)
{
    mp_impl->set_session_handler_factory(factory);
}

void model_context::set_table_handler(iface::table_handler* handler)
{
    mp_impl->set_table_handler(handler);
}

size_t model_context::get_string_count() const
{
    return mp_impl->get_string_count();
}

void model_context::dump_strings() const
{
    mp_impl->dump_strings();
}

const column_store_t* model_context::get_column(sheet_t sheet, col_t col) const
{
    return mp_impl->get_column(sheet, col);
}

const column_stores_t* model_context::get_columns(sheet_t sheet) const
{
    return mp_impl->get_columns(sheet);
}

model_iterator model_context::get_model_iterator(
    sheet_t sheet, rc_direction_t dir, const abs_rc_range_t& range) const
{
    return model_iterator(*this, sheet, range, dir);
}

abs_address_set_t model_context::get_all_formula_cells() const
{
    return mp_impl->get_all_formula_cells();
}

bool model_context::empty() const
{
    return mp_impl->empty();
}

}

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
